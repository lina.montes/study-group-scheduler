# Study Group Scheduler

Given a .csv export from whenisgood.net, this application analyzes the availability of participants and algorithmically determines the top 5 most optimal time slots, or if the proposed event is impossible given everyone’s schedule.